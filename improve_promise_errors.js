const add_method = require('add_method')


function ImprovePromiseErrors(init_error, remove_with=[]) {
  Object.getOwnPropertyNames(init_error).forEach(prop => {
    this[prop] = init_error[prop]
  })
  this.remove_with = remove_with
}

ImprovePromiseErrors.prototype = new Error

add_method('replaceStack', ImprovePromiseErrors, function(another_error) {
  const to_return = new ImprovePromiseErrors(another_error)

  to_return.stack = this.remove_with.reduce(
    (stack, to_remove) => stack.removeLinesWith(to_remove),
    this.stack.replace(/Error.*\n/g, 'Error: ' + another_error.message + '\n')
  )

  return to_return
})

add_method('removeLinesWith', String, function(substring) {
  return this
    .split('\n')
    .filter(Array.isArray(substring) ?
      line => !substring.every(ss => line.includes(ss)) :
      line => !line.includes(substring)
    )
    .join('\n')
})

module.exports = ImprovePromiseErrors

